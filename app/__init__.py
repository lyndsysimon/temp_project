from fastapi import FastAPI
from starlette.requests import Request

from app import config
from app.api import api_router
from app.db import Session


app = FastAPI(title=config.PROJECT_NAME)

app.include_router(api_router)

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = Session()
    response = await call_next(request)
    request.state.db.close()
    return response
