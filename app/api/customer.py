from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from app import models
from app import schemas
from app.api.utils import get_db


router = APIRouter()


@router.get("/", tags=["customer"], response_model=List[schemas.Customer])
def list_users(
    *,
    db: Session = Depends(get_db),
):
    return jsonable_encoder(db.query(models.Customer).all())


@router.get("/{customer_id}", tags=["customer"], response_model=schemas.Customer)
def get_user_by_id(
    *,
    db: Session = Depends(get_db),
    customer_id: int
):
    try:
        customer = db.query(models.Customer).filter_by(id=customer_id).one()
    except NoResultFound:
        raise HTTPException(status_code=404, detail="Customer not found")

    return jsonable_encoder(customer)
