from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import joinedload
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from app import models
from app import schemas
from app.api.utils import get_db


router = APIRouter()


@router.get("/connections", tags=["connection"], response_model=List[schemas.Connection])
def list_connections(
    *,
    db: Session = Depends(get_db),
    location_name: str = None,
    location_city: str = None,
    customer_name: str = None,
    connection_speed: str = None,
    connection_status: str = None,
):
    filters = []
    if location_name is not None:
        filters.append(
            models.Connection.location.has(models.Location.name == location_name)
        )
    if location_city is not None:
        filters.append(
            models.Connection.location.has(models.Location.city == location_city)
        )
    if customer_name is not None:
        filters.append(
            models.Connection.customer.has(models.Customer.name == customer_name)
        )
    if connection_speed is not None:
        filters.append(
            models.Connection.attributes.any(
                name="speed",
                value=connection_speed,
            )
        )
    if connection_status is not None:
        filters.append(
            models.Connection.attributes.any(
                name="status",
                value=connection_status,
            )
        )


    connections = db.query(
        models.Connection
    ).filter(*filters).all()

    return jsonable_encoder(connections)


@router.get("/connection/{connection_id}", tags=["connection"], response_model=schemas.Connection)
def get_connection_by_id(
    *,
    db: Session = Depends(get_db),
    connection_id: int
):
    # TODO: handle not found
    try:
        connection = db.query(
            models.Connection
        ).join(models.Customer).filter(
            models.Connection.id == connection_id
        ).one()
    except NoResultFound:
        raise HTTPException(status_code=404, detail="Connection not found")

    return jsonable_encoder(connection)

@router.post(
    "/connection",
    tags=["connection"],
    response_model=schemas.Connection,
    status_code=201,
)
def create_connection(
    *,
    db: Session = Depends(get_db),
    connection: schemas.ConnectionCreate,
):
    new_connection = models.Connection(
        description=connection.description,
        customer_id=connection.customer_id,
        location_id=connection.location_id,
    )
    db.add(new_connection)
    db.commit()
    for attribute in connection.attributes:
        db.add(
            models.ConnectionAttribute(
                connection_id=new_connection.id,
                name=attribute.name,
                value=attribute.value,
            )
        )
    db.commit()
    db.refresh(new_connection)
    return jsonable_encoder(new_connection)
