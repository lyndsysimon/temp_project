from typing import List

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from app import models
from app import schemas
from app.api.utils import get_db


router = APIRouter()


@router.get("/", tags=["location"], response_model=List[schemas.Location])
def list_locations(
    *,
    db: Session = Depends(get_db),
):
    return jsonable_encoder(db.query(models.Location).all())


@router.get("/{location_id}", tags=["location"], response_model=schemas.Location)
def get_location_by_id(
    *,
    db: Session = Depends(get_db),
    location_id: int
):
    try:
        location = db.query(models.Location).filter_by(id=location_id).one()
    except NoResultFound:
        raise HTTPException(status_code=404, detail="Location not found")
    return jsonable_encoder(location)
