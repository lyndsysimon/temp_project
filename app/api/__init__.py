from fastapi import APIRouter

from app.api import connection
from app.api import customer
from app.api import location


api_router = APIRouter()
# Note: This should be mounted at "/connection" for consistency, with subpath
#       routing handled in `/app/api/connection.py`. This is how I originally
#       had it, but have modified it to comply strictly with the provided
#       requirements.
api_router.include_router(connection.router, tags=["connection"])
api_router.include_router(customer.router, prefix="/customer", tags=["customer"])
api_router.include_router(location.router, prefix="/location", tags=["location"])
