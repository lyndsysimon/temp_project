import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.db import Base


class Connection(Base):
    __tablename__ = "connection"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    description = sa.Column(sa.Text, name="descripton", nullable=False)
    customer_id = sa.Column(sa.Integer, sa.ForeignKey("customer.id"))
    location_id = sa.Column(sa.Integer, sa.ForeignKey("location.id"))

    attributes = relationship(
        "ConnectionAttribute",
        back_populates="connection",
        lazy="joined",
    )
    customer = relationship(
        "Customer",
        back_populates="connections",
        lazy="joined",
    )
    location = relationship(
        "Location",
        back_populates="connections",
        lazy="joined",
    )


class ConnectionAttribute(Base):
    __tablename__ = "connection_attribute"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    connection_id = sa.Column(sa.Integer, sa.ForeignKey("connection.id"))
    name = sa.Column(sa.Text, nullable=False)
    value = sa.Column(sa.Text)

    connection = relationship("Connection", back_populates="attributes")


class Customer(Base):
    __tablename__ = "customer"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.Text, nullable=False)

    connections = relationship("Connection", back_populates="customer")


class Location(Base):
    __tablename__ = "location"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.Text, nullable=False)
    city = sa.Column(sa.Text, nullable=False)
    state = sa.Column(sa.Text)

    connections = relationship("Connection", back_populates="location")
