from typing import List

from pydantic import BaseModel


class Location(BaseModel):
    id: int
    name: str
    city: str
    state: str


class ConnectionAttributeCreate(BaseModel):
    name: str
    value: str


class ConnectionAttribute(BaseModel):
    id: int
    connection_id: int
    name: str
    value: str


class CustomerBase(BaseModel):
    name: str


class Customer(CustomerBase):
    id: int


class ConnectionCreate(BaseModel):
    description: str
    customer_id: int
    location_id: int
    attributes: List[ConnectionAttributeCreate]


class Connection(BaseModel):
    id: int
    description: str
    customer: Customer
    location: Location
    attributes: List[ConnectionAttribute]
