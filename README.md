# Usage

From the project root, to run the application server:

    pipenv run uvicorn app:app

Generated documentation is available at `http://localhost:8000/docs`. This also
allows convenient interactive exploration of the API.


# Notes

The column `connection.descripton` appears to have a typo. I treated this as an
implementation artifact, as I would likely not be able to update column names
for an existing table if this were a real project that was destined for
production. I hid the literal name behind the abstraction of the SQLAlchemy
model, and used the correctly-spelled version everywhere else.

The primary keys for the tables `customer`, `connection`, and
`connection_attribute` were not created using the `SERIAL` option in PostgreSQL.
I considered treating this as an implementation artifact, but decided that
implementing a key generation scheme in the application code was probably
outside the scope of is being evaluated. Outside of extraordinary circumstances,
it's also not something I would do in "real life". To resolve this, I created
the tables with autoincrementing primary keys and updated the provided SQL.

When creating a new connection (`POST /connection`), the client is expected to
provide a `location_id` and `customer_id`. There is currently no provision for
creating these objects through the API. If this were a real assignment, how I
would handle this would be dependent upon greater detail. If the connection must
be created with a single request, I'd require a minimal set of information for
those objects, then conditionally create records as needed if a matching record
did not exist. Otherwise, I would require a series of requests: one each to
create the appropriate `customer`, `location`, and `connection`. I prefer the
latter.
